#Solution to the following test task:
- A single command line argument containing a filename is passed to the program.
- The file is a text file of arbitrary (and possibly very large) size ( around 4 GB created by application).
- The file contains only “0” and “1” characters in random order.
- The program should read the file and determine and print the longest sequence of characters not containing 2 consecutive “0” characters.
- Example: Sequence 010**01110**01 – the bold characters (01110) form the longest sequence of characters matching the specification.
- The length of the longest sequence as well as the sequence itself should be printed to stdout.
- Please write clean code including error handling.
- Very much appreciated: Use parallel processing if possible.
- Use Java version 8.
- The program must run on a Core i7 and may not consume more than 2 GB of RAM.

#Build notes
It's a gradle project. Running gradle fatJar will build a runnable jar.  
Run example: java -jar braintags.jar my_file_with_data  

##Implementation details
There are 2 threads: 1 reads the file and sends parts of read data to the queue, where another thread loops through this data and finds the longest sequence without 2 consecutive zeroes.  
As the data comes in a single file, there is no need in more threads - reading thread will read the data slower than the looping one.  
But, if the data was represented as multiple smaller files on a distributed file system we would win from multiple reading and looping threads. Though that would require some additional complexity in implementation (as packets will not be read and looped sequentially).

##Benchmarks
On my machine with core i3 and SSD it takes around 30 seconds to find the longest sequence in a 4 gb file.

##Desirable improvements
If I had more time I would have add additional tests and sysout some progress information so that it's clear that the program is not stuck and doing some work.

##Possible optimization notes
It would be much more efficient to store zeroes and ones not as characters but as a bits, since we don't need more than a bit to represent the data.
So 4 gb file would become only 500 mb.