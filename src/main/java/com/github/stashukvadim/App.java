package com.github.stashukvadim;

import com.github.stashukvadim.parallel.ParallelSequenceFinder;
import com.google.common.base.Stopwatch;

import java.io.*;
import java.util.concurrent.TimeUnit;

/**
 * A single command line argument containing a filename is passed to the program.
 * - The file is a text file of arbitrary (and possibly very large) size ( around 4 GB created by application).
 * The file contains only “0” and “1” characters in random order.
 * - The program should read the file and determine and print the longest sequence of characters not
 * containing 2 consecutive “0” characters.
 * Example: Sequence 0100111001 – the bold characters (01110) form the longest sequence of
 * characters matching the specification.
 * - The length of the longest sequence as well as the sequence itself should be printed to stdout.
 */
public class App {
    public static void main(String[] args) {
        if (args.length == 0) {
            System.err.println("No path to file is provided. Please add it as a program argument");
            return;
        }
        try {
            Stopwatch stopwatch = Stopwatch.createStarted();
            String filePath = args[0];
            File file = new File(filePath);
            if (!file.exists()) {
                System.err.println("File " + file + " doesn't exist");
                return;
            }
            System.out.printf("File with data=%s, file size=%s bytes (%s Mb)\n", file, file.length(), file.length() / 1_000_000);

            FindSequenceResult findResult = findResult(file);
            printResults(file, findResult);

            System.out.println("Took seconds " + stopwatch.elapsed(TimeUnit.SECONDS));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static FindSequenceResult findResult(File file) throws IOException {
        SequenceFinder sequenceFinder = new ParallelSequenceFinder();
        return sequenceFinder.findLongestSequence(file);
    }

    private static void printResults(File file, FindSequenceResult findResult) throws IOException {
        String resultSequence = getSequence(file, findResult);
        System.out.println("The longest sequence length = " + findResult.getLength());
        System.out.println("The longest sequence:");
        System.out.println(resultSequence);
    }

    private static String getSequence(File file, FindSequenceResult findResult) throws IOException {
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file))) {
            is.skip(findResult.getStart());
            StringBuilder stringBuilder = new StringBuilder();
            for (int i = 0; i < findResult.getLength(); i++) {
                stringBuilder.append((char) is.read());
            }
            return stringBuilder.toString();
        }
    }
}
