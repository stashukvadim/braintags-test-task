package com.github.stashukvadim.parallel;

import com.github.stashukvadim.FindSequenceResult;
import com.github.stashukvadim.SequenceFinder;
import com.google.common.base.Preconditions;
import com.google.common.base.Stopwatch;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.*;

/**
 * This class starts FileReaderPacketProducer in a separate thread.
 * FileReaderPacketProducer reads a file by chunks and sends packets into a queue.
 * This class reads packets from a queue and analyzes them.
 */
public class ParallelSequenceFinder implements SequenceFinder {

    /**
     * 48 represents 0 character in ASCII, UTF-8 and some other encodings.
     * Current implementation assumes that file is encoded in ASCII or UTF-8
     */
    private static final int ZERO = 48;
    private static int BUFFER_SIZE = 10_000_000;//Experimenting with buffer size showed that 10 mb buffer is optimal
    private final BlockingQueue<Packet> queue = new ArrayBlockingQueue<>(1);


    @Override
    public FindSequenceResult findLongestSequence(File file) throws IOException {
        startPacketProducer(file);
        try {
            return findLongestSequence();
        } catch (InterruptedException interruptedException) {
            throw new RuntimeException(interruptedException);
        }
    }

    private void startPacketProducer(File file) {
        new Thread(new FileReaderPacketProducer(file, queue, BUFFER_SIZE)).start();
    }


    private FindSequenceResult findLongestSequence() throws IOException, InterruptedException {
        long longestStart = 0;
        long longestLen = 0;

        long start = 0;
        long len = 0;
        long curr = 0;
        int prevVal = -1;

        Packet packet;

        while (true) {
            packet = queue.poll(10, TimeUnit.SECONDS);
            if (packet == null){
                throw new RuntimeException("Timeout expired. No packet in queue present.");
            }

            if (packet.isEmpty()) {
                break;
            }

            byte[] buffer = packet.getBytes();
            for (int i = 0; i < packet.getLength(); i++) {
                int val = buffer[i];
                if (val == ZERO && prevVal == ZERO) {
                    if (longestLen < len) {
                        longestStart = start;
                        longestLen = len;
                    }
                    start = curr;
                    len = 0;
                }
                prevVal = val;
                len++;
                curr++;
            }
        }

        if (longestLen < len) {
            longestStart = start;
            longestLen = len;
        }

        return new FindSequenceResult(longestStart, longestLen);
    }
}
