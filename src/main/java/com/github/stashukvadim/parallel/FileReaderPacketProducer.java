package com.github.stashukvadim.parallel;


import java.io.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * This class reads from a file and sends packets to a queue.
 * Designed to run in a separate thread.
 */
class FileReaderPacketProducer implements Runnable {
    private final BlockingQueue<Packet> queue;
    private final File file;
    private final int packetSize;

    /**
     * @param file       File to read data from
     * @param queue      Queue to which send packets
     * @param packetSize packet size. Performance and memory will suffer if packet size is too small or too large.
     */
    FileReaderPacketProducer(File file, BlockingQueue<Packet> queue, int packetSize) {
        this.queue = queue;
        this.file = file;
        this.packetSize = packetSize;
    }

    @Override
    public void run() {
        try (BufferedInputStream is = new BufferedInputStream(new FileInputStream(file))) {
            int read;
            byte[] buffer = new byte[packetSize];
            while ((read = is.read(buffer)) != -1) {
                Packet packet = new Packet(buffer, read);
                queue.offer(packet, 10, TimeUnit.SECONDS);
            }
            queue.offer(new Packet(new byte[0], 0), 10, TimeUnit.SECONDS);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
