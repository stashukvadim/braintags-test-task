package com.github.stashukvadim.parallel;


/**
 * Represents read bytes from a file.
 * This class is needed for exchanging data using a queue between
 * FileReaderPacketProducer and ParallelSequenceFinder classes.
 */
class Packet {
    private final byte[] bytes;

    Packet(byte[] bytes, int length) {
        this.bytes = new byte[length];
        System.arraycopy(bytes, 0, this.bytes, 0, length);
    }

    byte[] getBytes() {
        byte[] defensiveCopy = new byte[bytes.length];
        System.arraycopy(bytes, 0, defensiveCopy, 0, bytes.length);
        return defensiveCopy;
    }


    int getLength() {
        return bytes.length;
    }

    boolean isEmpty(){
        return bytes.length == 0;
    }

    @Override
    public String toString() {
        return "Packet{" +
                ", bytes length=" + bytes.length +
                '}';
    }
}
