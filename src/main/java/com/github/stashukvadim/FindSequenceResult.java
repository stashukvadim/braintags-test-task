package com.github.stashukvadim;

import java.util.Objects;

/**
 * Represents the longest sequence from a given input where there are no 2 consecutive zeroes present.
 */
public class FindSequenceResult {
    private final long start;
    private final long length;

    public FindSequenceResult(long start, long length) {
        this.start = start;
        this.length = length;
    }

    /**
     * @return index of the first byte in a sequence
     */
    public long getStart() {
        return start;
    }


    /**
     * @return sequence length
     */
    public long getLength() {
        return length;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FindSequenceResult that = (FindSequenceResult) o;
        return start == that.start &&
                length == that.length;
    }

    @Override
    public int hashCode() {
        return Objects.hash(start, length);
    }

    @Override
    public String toString() {
        return "FindSequenceResult{" +
                "start=" + start +
                ", length=" + length +
                '}';
    }
}
