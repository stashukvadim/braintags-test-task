package com.github.stashukvadim;

import java.io.File;
import java.io.IOException;

/**
 * Implementation should find the longest sequence of characters not containing 2 consecutive “0” characters in a file.
 */
public interface SequenceFinder {

    /**
     * @param file File with "0" and "1" characters
     * @return FindSequenceResult that represents the longest sequence of characters not containing 2 consecutive “0” characters in a file.
     * @throws IOException in case any I/O exception occurs
     */
    FindSequenceResult findLongestSequence(File file) throws IOException;
}
