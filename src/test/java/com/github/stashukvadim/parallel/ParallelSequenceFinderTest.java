package com.github.stashukvadim.parallel;

import com.github.stashukvadim.FindSequenceResult;
import com.google.common.base.Stopwatch;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;

public class ParallelSequenceFinderTest {
    private static final byte ZERO = 48;//represents "0" char in UTF-8
    private static final byte ONE = 49;//represents "1" char in UTF-8

    private static long EXPECTED_LARGE_TEST_FILE_LENGTH = 4_000_000_000L + 1004;
    private static long LARGE_TEST_FILE_RANDOM_ZEROES_AND_ONES_LENGTH = 4_000_000_000L;
    private static int LARGE_TEST_FILE_CONSECUTIVE_ONES_LENGTH = 1000;
    private static int EXPECTED_LARGE_TEST_FILE_LONGEST_SEQUENCE_LENGTH = LARGE_TEST_FILE_CONSECUTIVE_ONES_LENGTH + 2;
    private static long EXPECTED_LARGE_TEST_FILE_LONGEST_SEQUENCE_START = LARGE_TEST_FILE_RANDOM_ZEROES_AND_ONES_LENGTH / 2 + 1;
    private static String LARGE_TEST_FILE_NAME = "test_4gb";

    private ParallelSequenceFinder finder = new ParallelSequenceFinder();

    /**
     * Filename convention: data_expectedStart_expectedLength
     */
    @Test
    public void shouldReturnCorrectFindSequenceResults() throws Exception {
        assertFileStartAndLength("empty", 0, 0);
        assertFileStartAndLength("0_0_0", 0, 1);
        assertFileStartAndLength("00_0_0", 0, 1);
        assertFileStartAndLength("1_0_0", 0, 1);
        assertFileStartAndLength("1011_0_4", 0, 4);
        assertFileStartAndLength("0100111001_3_5", 3, 5);
        assertFileStartAndLength("0101010111101_0_12", 0, 13);
    }

    private void assertFileStartAndLength(String filePath, long start, long length) throws IOException {
        assertThat(finder.findLongestSequence(getFile(filePath))).isEqualTo(new FindSequenceResult(start, length));
    }


    @Test
    public void shouldThrowExceptionWhenNonexistentFile() throws Exception {
        assertThatExceptionOfType(RuntimeException.class).isThrownBy(() -> finder.findLongestSequence(new File("nonexistent")));
    }

    /**
     * This test is needed to test the speed.
     * Will create a test 4gb file if it's not present.
     * Will NOT delete the file afterwards, so that the large test file can be used multiple times,
     * without the need to create it every time.
     */
    @Test
    @Ignore("Running this test will take some time and will create a 4gb file if it's not created yet.")
    public void performanceTest() throws Exception {
        File largeTestFile = new File(LARGE_TEST_FILE_NAME);

        createTestFileIfNeeded(largeTestFile);

        Stopwatch stopwatch = Stopwatch.createStarted();
        FindSequenceResult result = finder.findLongestSequence(largeTestFile);
        System.out.println("Result = " + result);
        System.out.printf("Finding sequence took %s seconds \n", stopwatch.elapsed(SECONDS));
        assertThat(result.getLength()).isEqualTo(EXPECTED_LARGE_TEST_FILE_LONGEST_SEQUENCE_LENGTH);
        assertThat(result.getStart()).isEqualTo(EXPECTED_LARGE_TEST_FILE_LONGEST_SEQUENCE_START);
    }

    private void createTestFileIfNeeded(File largeTestFile) throws IOException {
        if (largeTestFile.length() != EXPECTED_LARGE_TEST_FILE_LENGTH) {
            if (largeTestFile.exists()) {
                Files.delete(largeTestFile.toPath());
            }
            createLargeTestFile(largeTestFile, LARGE_TEST_FILE_RANDOM_ZEROES_AND_ONES_LENGTH);
        }
        System.out.printf("Large test file %s already exists, so will use it\n", largeTestFile);
    }

    private void createLargeTestFile(File file, long randomZeroesAndOnesLength) throws IOException {
        System.out.printf("Creating %s large test file with randomZeroesAndOnesLength=%s \n", file, randomZeroesAndOnesLength);
        Stopwatch stopwatch = Stopwatch.createStarted();
        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            writeRandomZeroesAndOnes(randomZeroesAndOnesLength / 2, outputStream);
            writeOnesInARowWithTwoZeroesAtStartAndEnd(outputStream, LARGE_TEST_FILE_CONSECUTIVE_ONES_LENGTH);
            writeRandomZeroesAndOnes(randomZeroesAndOnesLength / 2, outputStream);

            outputStream.flush();
        }
        System.out.printf("File %s with length=%s is created in %s seconds\n", file, file.length(), stopwatch.elapsed(SECONDS));
    }

    private void writeOnesInARowWithTwoZeroesAtStartAndEnd(BufferedOutputStream outputStream, long consecutiveOnes) throws IOException {
        System.out.println("writing only ones size = " + consecutiveOnes);
        outputStream.write(ZERO);
        outputStream.write(ZERO);
        for (int i = 0; i < consecutiveOnes; i++) {
            outputStream.write(ONE);
        }
        outputStream.write(ZERO);
        outputStream.write(ZERO);
    }

    private void writeRandomZeroesAndOnes(long length, BufferedOutputStream outputStream) throws IOException {
        System.out.println("writing random 0 and 1. Length = " + length);
        for (long i = 0; i < length; i++) {
            outputStream.write((Math.random() < 0.5 ? ZERO : ONE));
        }
    }

    private File getFile(String filePath) {
        ClassLoader classLoader = getClass().getClassLoader();
        URL resource = classLoader.getResource(filePath);
        if (resource == null) {
            throw new RuntimeException("Resource not found " + filePath);
        }
        return new File(resource.getFile());
    }

}